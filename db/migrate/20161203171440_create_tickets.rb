class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :name
      t.integer :amount
      t.float :price
      t.text :description

      t.timestamps
    end
  end
end
