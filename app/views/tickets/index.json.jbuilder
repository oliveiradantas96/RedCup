json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :name, :amount, :price, :description
  json.url ticket_url(ticket, format: :json)
end
